import { Component, OnInit, Input } from '@angular/core';
import { Socials } from './socials.model';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.sass']
})
export class SocialComponent implements OnInit {
@Input() social: Socials;
  constructor() { }

  ngOnInit() {
  }

}
