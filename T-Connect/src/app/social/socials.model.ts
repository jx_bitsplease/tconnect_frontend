
export class Socials {
    constructor(
        public facebookUrl?: string,
        public twitterUrl?: string,
        public linkedinUrl?: string,
        public githubUrl?:	string
    ) {}
}
