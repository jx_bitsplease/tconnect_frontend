import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileService } from './profile.service';
import { Profile } from './profile.model';
import { Skill } from '../skills/skill.model';
import { SkillsService } from '../skills/skills.service';
import { SkillsComponent } from '../skills/skills.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  profile: Profile;

  skills: {
    level?: number,
    skill?: Skill
  }[];

  @ViewChild(SkillsComponent, {static: false}) skillComp: SkillsComponent;
    

  constructor(
    private profileService: ProfileService,
    private skillService: SkillsService) { }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this.profileService.getProfile().subscribe(prof => {
      this.profile = prof
      if (!this.profile) {
        this.profile = { name: '', avatar: 'assets/images/harold_profile_pic.jpg', email: '', position: '', phone: '', socials: {}, deskNumber: '', roomNumber: '', profileSkills: []  }
      }
      console.log(this.profile);
    });
  }

  getSkills(){
    if (this.profile && this.profile.profileSkills){ 
    this.skillService.getSkills(this.profile.profileSkills).subscribe((res) => {
      this.skills = res;
      console.log(this.skills);
    });
    }
  }

  updateProfile() {
    // if (this.skillComp.skills){
    //   let asd:Subscription;
    //   this.skillComp.skills.map((res) => {
    //     if(!res.skills.id) {
    //         asd = this.skillService.createSkill(res.skills).subscribe((skill) => {
    //         res.skills.id = skill.body;
    //       })
    //     }
    //   })
    //   asd.add(() => {
    //     this.profile.profileSkills = this.skillService.getProfileSkills(this.skillComp.skills);
    //     console.log(this.profile.profileSkills);
    //   });
    // }
    if (this.profile.id){
      this.profileService.updateProfile(this.profile).subscribe((res) => {
      });
    } else {
      this.profileService.createProfile(this.profile).subscribe((res) => {
      });
    }

  }

}
