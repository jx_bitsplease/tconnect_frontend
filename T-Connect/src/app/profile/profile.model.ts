import { Socials } from '../social/socials.model';
import { Skill } from '../skills/skill.model';

export class Profile {
    constructor(
        public id?: string,
        public accountId?: string,
        public name?: string,
        public avatar?: string,
        public email?: string,
        public position?: string,
        public phone?: string,
        public socials?: Socials,
        public deskNumber?: string,
        public roomNumber?: string,
        public profileSkills?: {
            skillId?: string,
            level?: number}[]
    ) {}
}

