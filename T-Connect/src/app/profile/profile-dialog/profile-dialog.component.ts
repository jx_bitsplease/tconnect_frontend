import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { Profile } from '../profile.model';

@Component({
  selector: 'app-profile-dialog',
  templateUrl: './profile-dialog.component.html',
  styleUrls: ['./profile-dialog.component.sass']
})
export class ProfileDialogComponent implements OnInit {
  profile: Profile;

  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this.profileService.getProfile().subscribe(prof => {
      this.profile = prof
      if (!this.profile) {
        this.profile = { name: '', avatar: 'assets/images/harold_profile_pic.jpg', email: '', position: '', phone: '', socials: {}, deskNumber: '', roomNumber: '', profileSkills: []  }
      }
      console.log(this.profile);
    });
  }

}
