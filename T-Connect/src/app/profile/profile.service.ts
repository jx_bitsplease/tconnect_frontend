import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profile } from './profile.model';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private resourceUrl = 'https://da602lfs94.execute-api.us-east-1.amazonaws.com/Prod';
  private headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
  
  constructor(private http: HttpClient) { }

  getProfile(): Observable<Profile> {
    return this.http.get<Profile>(`${this.resourceUrl}/profile`, { headers: this.headers ,observe: 'response' }).pipe(map(res => res.body));
  }

  updateProfile(profile: Profile): Observable<HttpResponse<Profile>> {
    return this.http.put<Profile>(`${this.resourceUrl}/profile/${profile.id}`, profile, { headers: this.headers ,observe: 'response' }).pipe(map(res => res));
  }

  createProfile(profile: Profile): Observable<HttpResponse<Profile>> {
    return this.http.post<Profile>(`${this.resourceUrl}/profile`, profile, { headers: this.headers ,observe: 'response' }).pipe(map(res => res));
  }
}

