import { Component, OnInit } from '@angular/core';
import { CognitoUserPool, AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  poolData = {
    UserPoolId : 'us-east-1_A4BjAXoe4',
    ClientId : '76h9qfa4gn2e8dena6hg21iodg'
  };

  userPool = new CognitoUserPool(this.poolData);

  username: string;
  password: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  callLogin(){
    this.login().subscribe((res) => {
      if (res.type === 'success') {
        this.redirect();
      }
    })
  }

  login(): Observable<{ type: string, result: any }> {
    const authenticationDetails = new AuthenticationDetails({
      Username : this.username,
      Password : this.password,
    });

    const userData = {
      Username: this.username,
      Pool: this.userPool
    };

    const cognitoUser = new CognitoUser(userData);
    return new Observable<{ type: string, result: any}>(obs => {
      cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: (result: any) => {
        console.log('access token', result.getAccessToken().getJwtToken());
        console.log('id token', result.getIdToken().getJwtToken());
        localStorage.setItem('token', result.getIdToken().getJwtToken());
        console.log('refresh token', + result.getRefreshToken().getToken());
        obs.next({ type: 'success', result: result });
        obs.complete();
      },
      onFailure: (err: any) => {
        console.log(err);
        obs.error(err);
      },
      newPasswordRequired: (userAttributes) => {
        const newPassword = 'Kecske69'
        delete userAttributes.email_verified;
        obs.complete();
        obs.next({ type: 'newPasswordRequired', result: [userAttributes, newPassword] });
      }
    });
  });
}

  redirect() {
    this.router.navigate(['/home']);
  }

}
