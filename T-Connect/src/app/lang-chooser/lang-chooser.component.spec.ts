import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LangChooserComponent } from './lang-chooser.component';

describe('LangChooserComponent', () => {
  let component: LangChooserComponent;
  let fixture: ComponentFixture<LangChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LangChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LangChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
