
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-lang-chooser',
  templateUrl: './lang-chooser.component.html',
  styleUrls: ['./lang-chooser.component.sass']
})
export class LangChooserComponent implements OnInit {

  languages: any;
  currentlang = localStorage.getItem('lang');

  constructor(public translate: TranslateService) { }

  ngOnInit() {
    this.languages = this.translate.getLangs().map(val => {
      return { label: val, value: val };
    });
  }

  change(data) {
    this.translate.use(data.value);
    localStorage.setItem('lang', data.value);
  }

}
