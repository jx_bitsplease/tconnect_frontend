export class ArrayOfRooms {
    constructor(
       public rooms?: Room[]
    ) {}
}

export class Room {
    constructor(
        public roomId?: string,
        public roomName?: string
    ) {}
}