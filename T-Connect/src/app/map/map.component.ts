import { Component, OnInit } from '@angular/core';
import { MapService } from './map.service';
import { ArrayOfRooms, Room } from './arrayOfRooms.model';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {

  arrayOfRooms: {}[];

  constructor(
    private mapService: MapService
  ) { }

  ngOnInit() {
    this.getRooms();
  }

  getRooms() {
    this.mapService.getRooms().subscribe((res) => {
      this.arrayOfRooms = res.rooms.map((room) => {
        return {
          label: room.roomName,
          value: room.roomId
        }
      })
      console.log(this.arrayOfRooms);
    });
  }

}
