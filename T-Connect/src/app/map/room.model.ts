export class Room {
    constructor(
        public roomId?: string,
        public roomName?: string,
        public level?: number,
        public capacity?: number,
        public isBooked?: boolean,
        public bookedFrom?: string,
        public bookedUntil?: string
    ) {}
}