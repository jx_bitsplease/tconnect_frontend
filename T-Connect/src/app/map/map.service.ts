import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ArrayOfRooms } from './arrayOfRooms.model';
import { map } from 'rxjs/operators';
import { Room } from './room.model';
import { BookingRequest } from './bookingRequest.model';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private resourceUrl = 'https://da602lfs94.execute-api.us-east-1.amazonaws.com/Prod';
  private headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
  
  constructor(private http: HttpClient) { }

  getRooms(): Observable<ArrayOfRooms> {
    return this.http.get<ArrayOfRooms>(`${this.resourceUrl}/rooms`, { headers: this.headers ,observe: 'response' }).pipe(map(res => res.body));
  }

  getRoom(id:number): Observable<Room> {
    return this.http.get<Room>(`${this.resourceUrl}/room/${id}`, { headers: this.headers ,observe: 'response' }).pipe(map(res => res.body));
  }

  createBooking(id:number, bookingRequest: BookingRequest): Observable<HttpResponse<BookingRequest>> {
    return this.http.post<BookingRequest>(`${this.resourceUrl}/room/${id}/booking`, bookingRequest, { headers: this.headers ,observe: 'response' }).pipe(map(res => res));
  }
}
