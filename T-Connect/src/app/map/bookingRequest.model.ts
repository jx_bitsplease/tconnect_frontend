export class BookingRequest {
    constructor(
        public from?: string,
        public until?: string,
        public requiredCapacity?: number
    ) {}
}