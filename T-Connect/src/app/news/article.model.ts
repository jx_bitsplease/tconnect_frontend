import { ArticleTags } from './article-tags.model';

export class Article {
    constructor(
        public id?: string,
        public title?: string,
        public author?: string,
        public date?: string,
        public content?: string,
        public tags?: ArticleTags
    ) {}
}