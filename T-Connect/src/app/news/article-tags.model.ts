export class ArticleTags {
    constructor(
        public important?: boolean,
        public it?: boolean,
        public sales?: boolean,
        public management?: boolean,
        public hr?: boolean,
        public maintenance?: boolean,
        public cleaning?: boolean,
        public network?: boolean,
        public qa?: boolean,
        public marketing?: boolean,
        public field?: boolean,
        public other?: boolean
    ) {}
}