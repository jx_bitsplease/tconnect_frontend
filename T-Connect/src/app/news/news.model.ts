import { Article } from './article.model';
import { ArticleTags } from './article-tags.model';

export class News {
    constructor(
        public id?: string,
        public title?: string,
        public articles?: Article[],
        public tags?: ArticleTags
    ) {}
}