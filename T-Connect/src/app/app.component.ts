import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'T-Connect';

  readonly defaultLang = 'hu';

  constructor(
    public translate: TranslateService
  ){
    translate.addLangs(['en', this.defaultLang]);
    if (localStorage.getItem('lang')) {
      translate.setDefaultLang(localStorage.getItem('lang'));
    } else {
      translate.setDefaultLang(this.defaultLang);
      localStorage.setItem('lang', this.defaultLang);
    }
  }
}
