import { Injectable } from '@angular/core';
import { SKILLS } from '../profile/mock-profiles';
import { Observable, of } from 'rxjs';
import { Skill } from './skill.model';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  private resourceUrl = 'https://da602lfs94.execute-api.us-east-1.amazonaws.com/Prod';
  private headers = new HttpHeaders().set('Authorization', localStorage.getItem('token'));
  
  constructor(private http: HttpClient) { }

  getSkills(skills?: {
    skillId?: string,
    level?: number,
    }[]): Observable<{level?: number,
    skills?: Skill}[]> {
      console.log('asd');
     let ids = skills.map((skillId) => {
       return skillId.skillId
      });
      return this.http.post<{level?: number,
        skills?: Skill}[]>(`${this.resourceUrl}/skill/list`, ids, { headers: this.headers ,observe: 'response' }).pipe(map(res => res.body));
  }

  getProfileSkills(skill: {
    level?: number,
    skills?: Skill
    }[]){
      let profileSkills: {
        skillId?: string,
        level?: number}[] = [];
        skill.forEach((res) => {
          let skillId = res.skills.id;
          let level = res.level;
           profileSkills.push({skillId, level});
        });
        return profileSkills;
  }

  createSkill(skill: Skill): Observable<HttpResponse<string>> {
    return this.http.post<string>(`${this.resourceUrl}/skill`, skill, { headers: this.headers ,observe: 'response' }).pipe(map(res => res));
  }

}
