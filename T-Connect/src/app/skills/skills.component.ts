import { Component, OnInit, Input } from '@angular/core';
import { Skill } from './skill.model';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.sass']
})
export class SkillsComponent implements OnInit {
  @Input() skills?: {level?: number, skills?: Skill}[];
  defaultValue: {level?: number, skills?: Skill}[] = [{level: 1, skills: {}}];

  constructor() { }

  ngOnInit() {
  }

  addSkill(){
    if (!this.skills) {
      this.skills = [];
    }
    this.skills.push({level: 1, skills: new Skill()});
  }

  deleteSkill(index) {
    this.skills.splice(index, 1);
  }

}
