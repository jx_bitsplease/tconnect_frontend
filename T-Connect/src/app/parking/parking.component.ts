import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.sass']
})
export class ParkingComponent implements OnInit {
  display = false;
  constructor() { }

  ngOnInit() {
  }

  open() {
    this.display = true;
  }

}
